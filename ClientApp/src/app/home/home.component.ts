import { Component } from '@angular/core';
import { Machine } from '../machine';
import { Bench } from '../bench';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  machines: Machine[];
  benches: Bench[];

  constructor() {
    this.machines = [];

    for (var i = 0; i < 100; i++) {
      this.machines.push({ id: i, name: 'Machine' + i.toString() }); 
    }

    this.benches = [];

    for (var i = 0; i < 10; i++) {
      this.benches.push({ id: i, name: 'Bench' + i.toString() });
    }
  }
}
